package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;
//comments
public class CounterTest {
    @Test
    public void testDivide() throws Exception {

        final int result= new Counter().divide(10,5);
        assertEquals("Divide", 2, result);
        
    }

@Test
    public void testDivideZero() throws Exception {

        final int result= new Counter().divide(10,0);
        assertTrue(result>0);
        
    }
    
}
